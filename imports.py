from __future__ import division
from printf import printf
import sys
import os
import string
import math
from collections import Counter
import operator
import re, readline
import nltk
from nltk.tokenize import sent_tokenize
from nltk.corpus import stopwords
from nltk.stem.porter import *
from nltk.stem.snowball import SnowballStemmer
import nltk.tag, nltk.data

tagger = nltk.data.load(nltk.tag._POS_TAGGER)
global title
global feats
global text
text = ""
feats = {}