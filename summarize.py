from imports import *

## Method to split text into sentences and then words and do further preprocessing on it ##
def preprocessText(text):
	try:
		tokens = nltk.word_tokenize(text)
		filtered_words = [w for w in tokens if not w in stopwords.words('english')]
		global word_count
		word_count = Counter(filtered_words)
		sents = sent_tokenize(text)
		tokenized = []
		stemmer = SnowballStemmer("english")
		for sent in sents:
			tokenized.append([stemmer.stem(w) for w in nltk.word_tokenize(sent) if not w in stopwords.words('english')])
		global long_sentence
		long_sentence = max(len(sentence) for sentence in tokenized)
	
		return tokenized
	except:
		print ">> Error during pre-processing"
		quit()

## Returns ratio of title words occuring in a sentence ##
def titleFeature(sents):
	try:
		title_feat = []
		for sentence in sents:
			count = 0
			for word in sentence:
				if word in title:
					count+=1
			title_feat.append(count/len(title))
		return title_feat
	except:
		print ">> Error during title feature extraction"
		quit()
## Returns ratio of sentence length to longest sentence length ##
def sentenceLength(sents):
	try:
		sent_length = []
		for sentence in sents:
			sent_length.append(len(sentence)/long_sentence)
		return sent_length
	except:
		print ">> Error during sentence length feature extraction"
		quit()

## Returns ratio tf-isf of a word in the sentence over the max tf-isf ##
def tf_isf(sents):
	try:
		sf = []
		for sentence in sents:
			w=0
			for word in sentence:
				n=0
				for sent in sents:
					if word in sent:
						n+=1
				w += word_count[word] * math.log(len(sents)/n)
			sf.append(w)
		sf[:] = [x / max(sf) for x in sf]
		return sf
	except:
		print ">> Error during tf-isf feature extraction"
		quit()

## Rates the sentence based on its position in the para ##
def sent_pos(sents):
	try:
		n=5
		sp=[]
		for sentence in sents:
			if n > 0:
				sp.append(n/5)
				n-=1
			else: sp.append(0)
		return sp
	except:
		print ">> Error during sentence position feature extraction"
		quit()

## Utility method to get cosine similarity between two sentences ##
def get_cosine(vec1, vec2):
	try:
		intersection = set(vec1.keys()) & set(vec2.keys())
		numerator = sum([vec1[x] * vec2[x] for x in intersection])
		
		sum1 = sum([vec1[x]**2 for x in vec1.keys()])
		sum2 = sum([vec2[x]**2 for x in vec2.keys()])

		denominator = math.sqrt(sum1) * math.sqrt(sum2)
		if not denominator:
			return 0.0
		else:
			return float(numerator) / denominator
	except:
		print ">> Error during cosine vector extraction"
		quit()
	          
## Returns ratio of cosine similarity of a sentence with every other sentence over the max similarity score ##
def sent_sim(sents):
	try:
		sim = []
		for i in range(len(sents)):
			s = 0
			for j in range(len(sents)):
				if i == j:
					continue
				s += get_cosine(Counter(sents[i]), Counter(sents[j]))
			sim.append(s)
		sim[:] = [x / max(sim) for x in sim]
		return sim
	except:
		print ">> Error during sentence similarity feature extraction"
		quit()

## Returns ratio of proper nouns occuring in a sentence over the length of the sentence ##
def proper_noun(sents):
	try:
		proper = []
		for sent in sents:
			sentence = nltk.word_tokenize(sent)
			count = 0
			pos = tagger.tag(sentence)
			for p in pos:
				if p[1] == ('NNP' or 'NNPS'):
					count += 1
			proper.append(count/len(sentence))
		return proper
	except:
		print ">> Error during proper noun feature extraction"
		quit()


## returns the ration of thematic words occuring in a sentence over the max thematic words occuring in any sentence ##
def thematic(sents):
	try:
		theme = sorted(word_count.items(), key=operator.itemgetter(1), reverse = True)
		# print theme
		thematic_words = []

		count = 0
		for word, occurence in theme[:10]:
			if not word.isalnum():
				continue
			else:
				thematic_words.append(word)
				count+=occurence
		count = 0
		thematic_score = []
		for sentence in sents:
			count = 0
			for word in sentence:
				if word in thematic_words:
					count+=1
			thematic_score.append(count)
		thematic_score[:] = [x / max(thematic_score) for x in thematic_score]
		return thematic_score
	except:
		print ">> Error during thematic words feature extraction"
		quit()

## Returns the ratio of numerical data occuring in a sentence over the length of the sentence ##
def num_data(sents):
	try:
		num_score = []
		for sentence in sents:
			count = 0
			for word in sentence:
				if word.isdigit():
					count+=1
			num_score.append(count/len(sentence))
		return num_score
	except:
		print ">> Error during numerical data feature extraction"
		quit()


## Extracts all the neccessary eight feautures of each sentence and stores in a labelled dictionary ##
def extractSentenceFeatures(sents):
	feats['title_feature'] = titleFeature(sents)
	feats['sentence_length'] = sentenceLength(sents)
	feats['tf-isf'] = tf_isf(sents)
	feats['sentence_position'] = sent_pos(sents)
	feats['sentence_similarity'] = sent_sim(sents)
	feats['proper_nouns'] = proper_noun(sent_tokenize(text))
	feats['thematic_words'] = thematic(sents)
	feats['numerical_data'] = num_data(sents)

## Method to get the GSM score of a sentence ##
def gsm(feats, n):
	gsm_dict = {}
	for i in range(n):
		gsm_list = []
		for key in feats.keys():
			gsm_list.append(feats[key][i])
		gsm_dict[i] = sum(gsm_list)
	return gsm_dict

##  Method to print out the summary of the text using the GSM method ##
def summary(score, text):
	global short_sum
	sort = sorted(score.items(), key=operator.itemgetter(1), reverse = True)
	n = math.ceil(len(sort)*0.2)
	sort = sort[:int(n)]
	a_sort= sorted(sort, key = lambda element : element[0])

	sents = sent_tokenize(text)
	for i in range(int(n)):
		short_sum += sents[a_sort[i][0]] 
		printf(sents[a_sort[i][0]] + " ")
					
## Driver code  ##	

#Extracting name of file from the path
filename = os.path.basename(sys.argv[1])
filename = os.path.splitext(filename)

# Extract title from file
# raw_title = raw_input("Enter title: ").split( )
with open(sys.argv[1]) as f:
    raw_title = f.readlines()[0]
f.close()

# Create summary file in the summary folder and write the title name into it
sum_file = open(("models/Summaries/" + str(filename[0]) + "_Summary.txt"), "w")
sum_file.write(str(raw_title))

# Split title into list of stemmed words without stopwords
print "Title - " + str(raw_title)
raw_title = raw_title.split( )
title = []
for word in raw_title:
	if word not in stopwords.words('english'):
		title.append(SnowballStemmer("english").stem(word))

# Extract body from file 
# text = raw_input("Enter body: ")
with open(sys.argv[1]) as f:
	raw_text = f.readlines()[2:]
	
for para in raw_text:
	text += para
#print text

paras = text.split("\n\n")

short_sum = ""
print "Summmary is:- "

for para in paras:
	# print para
	if  para:
		sents = preprocessText(para)
		# Driver code to extract sentence features over the whole text
		extractSentenceFeatures(sents)
		'''for key in feats.keys():
			print key + " - " + str(feats[key])'''

		gsm_score = gsm(feats, len(sents))
		summary(gsm_score, para)
	else:
		print ""
print short_sum
sum_file.write(short_sum)