# README #

Summarize.py

### What is this repository for? ###

* This engine helps you to summarize your text. It is based on the algorithms published in the paper mentioned below. It gives you a minimum 20% extraction rate
* Language used - Python 2.7.10
* Reference - http://arxiv.org/pdf/0906.4690.pdf
* Libraries used - __future__, sys, string, math, collections, operator, re, readline, nltk

### How do I get set up? ###

* Summary of set up
	Run - python(2.7) summarrize.py 'path to file where the text is stored'
* Text file format - The data should be stored in a .txt file. First line of file should contain the Title followed by its respective Body from the third line. All the paragraphs must be separated by two carriage returns or "\n\n"
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner - Chetan Jaydeep(https://bitbucket.org/Chetan_WiredIn)
		

